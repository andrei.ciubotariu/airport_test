# Andrei Ciubotariu Gheorghe

### Installation

1. Open terminal in desired location for the project
2. Clone with HTTPS - comand " git clone https://gitlab.com/andrei.ciubotariu/airport_test.git "
3. Change directory to airport_test  -comand " cd airport_test "
4. Open Cypress app -comand " npx cypress open "
5. Select e2e testing and run desired test

## Test 1: Airportlabs Website

### 1. Check Home Page Title
- Action: Click the cookie button if present.
- Action: Validate that the "Engineered for aviation" text is visible and contains the expected text.
- Action: Validate that the "designed for people." text is visible and contains the expected text.
- Action: Validate the expected CSS properties for certain elements on the page.

### 2. Check Our Activity Section
- Action: Validate that the "Our Activity in numbers" heading is present and contains the expected text.
- Action: Validate that the heading is visible.
- Action: Validate the expected CSS properties for the heading.

### 3. Check Contact Form on Home Page
- Action: Click the "Contact" link in the navigation bar.
- Action: Validate that the URL matches the expected contact page URL.
- Action: Fill in various fields of the contact form with test values.
- Action: Validate that form fields have the expected visibility and placeholder attributes.
- Action: Validate the appearance and background color of the "Submit Application" button.

### 4. Check Social Media Links
- Action: Validate that each social media link in the "div-block-60" section has the expected attributes.
- Action: Validate that the link has the target attribute set to "_blank".
- Action: Validate that the link has the expected class attribute.
- Action: Validate that the link's href matches the expected URL.

### 5. Check Logo Image
- Action: Validate that the logo image in the "desktop-brand" class is visible.
- Action: Get the width and height of the logo image.
- Action: Validate that the logo image has the expected width and height.
- Action: Validate that the logo image's source URL matches the expected URL.

## Test 2: Add to Shopping Cart TV and Accessory

### 1. Add Most Expensive TV and Most Affordable Remote to Cart
- Action: Click the "Accept" button if visible.
- Action: Click the "Dismiss" button for login notice if visible.
- Action: Search for a TV product, filter by brand, stars, sort option, and stock option.
- Action: Retrieve and compare prices of relevant products using the specified comparison function.
- Action: Search for a remote control product, filter by brand, stars, sort option, and stock option.
- Action: Retrieve and compare prices of relevant products using the specified comparison function with ascending order.
- Action: Check the contents of the cart.

### 2. Search and Filter Functionality
- Action: Clear and type the search term in the search box.
- Action: Validate that the suggested search result appears and contains the expected text.
- Action: Filter the search results by brand, stars, and stock option.
- Action: Filter the results by additional category if provided.
- Action: Scroll to the top of the page.
- Action: Open the sort options dropdown and choose a sort order.

### 3. Get and Compare Prices Functionality
- Action: Get the price of the first product on the page.
- Action: Calculate the width percentage and rating value of star ratings.
- Action: For each product with a suitable rating, get and store its price.
- Action: Compare the stored prices with the first product's price using the specified comparison function.
- Action: Add the most expensive product to the cart if comparison succeeds, otherwise throw an error.

### 4. Check Cart Functionality
- Action: Click the cart icon to view the cart.
- Action: Validate that the "Telecomanda LG" product is visible in the cart.
- Action: Validate that the "Televizor LG" product is visible in the cart.
