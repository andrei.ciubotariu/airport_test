export const homeTitle = [
  {
    selector: '.header-text > .home',
    assertions: [
      { property: 'text-align', value: 'center' },
      { property: 'font-size', value: '72px' },
      { property: 'width', value: '800px' },
    ],
  },
  {
    selector: '.header-text > .blue',
    assertions: [
      { property: 'text-align', value: 'center' },
      { property: 'font-size', value: '72px' },
      { property: 'color', value: 'rgb(178, 177, 255)' },
      { property: 'width', value: '800px' },
    ],
  },
];

export const ourActivityTitle = [
  { property: 'font-size', value: '40px' },
  { property: 'color', value: 'rgb(255, 255, 255)' },
  { property: 'text-align', value: 'left' },
  { property: 'flex-wrap', value: 'wrap' },
  { property: 'justify-content', value: 'flex-start' },
  { property: 'font-size', value: '40px' },
  { property: 'font-weight', value: '300' },
  { property: 'text-decoration', value: 'none solid rgb(255, 255, 255)' },
  { property: 'display', value: 'flex' },
  { property: 'position', value: 'static' },
];