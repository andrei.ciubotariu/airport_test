import * as constants from '../support/constants';
import { homeTitle, ourActivityTitle } from  '../support/css';

describe('test airportLabs website', () => {
  
  beforeEach(() => {                   
    cy.visit(constants.airportUrl)
    cy.get('.cookie > .button-text').click();
  })

  it('checks home page title', () => {
    // check home page title
    cy.get('.header-text > .home')
    .contains('Engineered for aviation,')
    .should('be.visible')
    .should('contain', 'Engineered for aviation,');

  cy.get('.header-text > .blue')
    .should('be.visible')
    .should('contain', 'designed for people.');
  
    // check home page title css
    homeTitle.forEach(item => {
      // loop through each item in the array and check the css
      cy.get(item.selector).should('be.visible').and($element => {
        item.assertions.forEach(assertion => {
          expect($element).to.have.css(assertion.property, assertion.value);
        });
      });
    });
  });

  it('checks Our Activity section', () => {
    // check Our Activity section title
    cy.get('.our-impact > .container-8 > .h2').should($element => {
      expect($element).to.contain('Our Activity in numbers');
      expect($element).to.be.visible;

      // check Our Activity section title css
      ourActivityTitle.forEach(assertion => {
        expect($element).to.have.css(assertion.property, assertion.value);
      });
    });

    // Select the first stats block containing the user count
    cy.get('.div-block-24').eq(0).within(() => {
      cy.get('h2.h2.green').should('have.text', '200k');
      cy.get('h4.h4').should('have.text', 'Users worldwide');
      cy.get('h2.h2.green').should('have.css', 'color', 'rgb(82, 206, 147)');
    });
  })

  it('checks contact form on home page', () => {
    // check contact form on home page
    cy.get('.nav-link---contact > .navbar-text').click();
    cy.url().should('eq', constants.airportUrl + 'other/contact');
  
    // Check the form title and description text on the contact page
    cy.fixture('formFields.json').then((formFields) => {
      formFields.forEach((field) => {
        cy.get(field.id)
          .should('be.visible')
          .and('have.attr', 'placeholder', field.placeholder)
          .type(field.value);
      });
    });
  
    // Check the submit button
    cy.get('input[type="submit"][value="Submit Application"]')
      .should('be.visible')
      .and('have.attr', 'value', 'Submit Application')
      .should('have.css', 'background-color', 'rgb(94, 100, 255)');
  });

  it('should check all social media links', () => {
    cy.get('.div-block-60 a').each((socialLink) => {
      const linkUrl = socialLink.attr('href');

      cy.wrap(socialLink).should('have.attr', 'target', '_blank'); 
      cy.wrap(socialLink).should('have.attr', 'class', 'logo-footer-container w-inline-block'); 
      cy.wrap(socialLink).should('have.attr', 'href', linkUrl); 
      cy.wrap(linkUrl).should('eq', linkUrl);
      
    });
  });

  it('should check logo image', () => {
    cy.get('.desktop-brand img').should('be.visible').then(($img) => {
      const width = $img.width();
      const height = $img.height();
  
      expect(width).to.eq(140);
      expect(height).to.eq(39.6094); 
      cy.wrap($img).should('have.attr', 'src', 'https://uploads-ssl.webflow.com/621780e23ce4730dbde38ef2/6218c570693ffd35d9005f03_AL_Logo_White_Horizontal.svg');
    });
  });
})