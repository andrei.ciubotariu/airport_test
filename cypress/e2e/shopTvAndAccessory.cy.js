import * as constants from '../support/constants'
import productsData from '../fixtures/productData.json'

describe('test shopping functionality emag', () => {

  before(() => {
    // Visit the emag website and disable uncaught exception handling
    cy.visit(constants.emagUrl);
    cy.setUpEmag()
  })

  it('should add the most expensive tv and most cheap remote to cart', () => {
    // Click the "Accept" button if visible and dismiss the login notice.
    cy.get('.col-sm-5 > .btn-primary').should('be.visible').contains('Accept').click()
    cy.get('.js-dismiss-login-notice-btn > .em').should('be.visible').click()

    // Search for TV products, filter, and compare prices.
    const tvData = productsData.tv
    searchAndFilter
    (
      tvData.product, 
      tvData.brand, 
      tvData.category, 
      tvData.stars, 
      tvData.sortOption, 
      tvData.stockOption
    )
    getAndComparePrices(tvData.product, Math.max);

    // Search for remote control products, filter, and compare prices.
    const remoteData = productsData.remote;
    searchAndFilter
    (
      remoteData.product, 
      remoteData.brand, 
      remoteData.category, 
      remoteData.stars, 
      remoteData.sortOption, 
      remoteData.stockOption
    )
    getAndComparePrices(remoteData.product, Math.min, true)

    // Check the cart.
    checkCart()
  })

  function searchAndFilter(product, brand, category, stars, sortOption, stockOption) {
    // Search for the product and click on the suggested title.
    cy.get('#searchboxTrigger').should('be.visible').clear().type(product)
    cy.get('.searchbox-suggestion-title').contains(product).should('be.visible').click();

    // Filter the products by brand, category, stars, and stock.
    cy.get('.filter-body .js-scrollable > [data-type="brand"], [data-name="' + brand + '"]')
      .should('be.visible').contains(brand).click({force: true})
    cy.get('.filter-body .js-scrollable > [data-type="custom_filter"], [data-option-id="' + stars + '"]')
      .should('be.visible').click({force: true})
    if (category) {
      cy.get('.filter-body .js-scrollable > [data-type="custom_filter"], [data-name="' + category + '"]')
        .should('be.visible').contains(category).click({force: true})
    }
    cy.get('.js-filter-item > .filter-item, [data-option-id="stock"], [data-name="' + stockOption + '"]')
      .should('be.visible').click({ force: true })
      cy.wait(3000)

    // Open the sort dropdown and select the sort option
    cy.scrollTo('top')
    cy.get(':nth-child(2) > .sort-control-btn-dropdown > .btn > .sort-control-btn-option').click({force: true})
    cy.get('.js-sort-option, [data-sort-dir="' + sortOption + '"]').contains(sortOption).click({ force: true })
    
    // wait for the page to load
    cy.interceptAllRequests()
    // cy.wait(3000);
  }

  function getAndComparePrices(productType, priceComparison, ascending = false) {
    // Get the price of the first product and compare it with the rest of the products.
    cy.get('.card-v2-pricing > .product-new-price').first().invoke('text').then((priceText) => {
      const firstProductPrice = parseFloat(priceText.replace(' Lei', '').replace(',', '.').replace('de la', ''))
      
      let prices = []

      // Get the prices of the rest of the products and compare them with the first product.
      cy.get('.card-v2-rating .star-rating-inner').each(($starRating) => {
        // Get the rating value of the product.
        const widthPercentage = parseFloat($starRating.attr('style').split('width: ')[1])
        // Calculate the rating value.
        const ratingValue = (widthPercentage / 100) * 5
        
        if (ratingValue >= 3) {
          const $productCard = $starRating.parents('.card-v2')

          // Get the price of the product and add it to the prices array.
          cy.wrap($productCard).find('.card-v2-pricing > .product-new-price').invoke('text').then((priceText) => {
            const price = parseFloat(priceText.replace(' Lei', '').replace(',', '.').replace('de la', ''))
            prices.push(price)
          })
        }
      }).then(() => {
        // Compare the prices of the products.
        const comparisonResult = firstProductPrice === priceComparison(...prices)
        if (comparisonResult) {
          // Add the product to the cart.
          cy.get('.card-v2 > .card-v2-wrapper > .card-v2-content > .card-v2-atc > form > .btn')
          .first().should('be.visible').click()
          cy.get('.modal-header > [data-dismiss="modal"]').should('be.visible').click()
        } else {
          // Throw an error if the comparison result is false.
          const comparisonType = ascending ? 'least expensive' : 'most expensive'
          throw new Error(`The ${productType} product is not the ${comparisonType} one`)
        }
      });
    });
  }

  function checkCart() {
    // Open the cart and check if the products are there.
    cy.get('#my_cart').should('be.visible').click()
    cy.contains('Telecomanda LG').should('be.visible')
    cy.contains('Televizor LG').should('be.visible')
  }
});